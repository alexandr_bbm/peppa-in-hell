import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {Game as GameView} from './view/Game'
import {Game} from './models/Game'

game = new Game();

ReactDOM.render(<GameView game={game} />, document.getElementById('app'))