import * as React from 'react'
import {Container} from '@inlet/react-pixi'

import Rectangle from '../primitives/Rectangle'
import {Text} from '../primitives/Text.cjsx'

hpBarHeight = 30

export HpBar = ({hp, fullHp, x = 0, y = 0, width}) ->
  hpBarFrame =
    <Rectangle
      height={hpBarHeight}
      width={width}
      strokeColor={0xffffff}
    />

  hpWidth = hp * width / fullHp
  hpBar =
    <Rectangle
      height={hpBarHeight}
      width={hpWidth}
      fill={0x228b22}
    />

  return (
    <Container
      x={x}
      y={y}
    >
      {hpBar}
      {hpBarFrame}
      <Text
        text={"#{hp}/#{fullHp}"}
        x={50}
        y={1}
      />
    </Container>
  )