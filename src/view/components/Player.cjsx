import * as React from 'react'
import {Sprite, Container} from '@inlet/react-pixi'

import {formatNameLevel} from '../../models/utils/formatters'
import {spriteUtils} from '../utils/pixi'
import coinImage from '../../img/coin.png';
import {Text} from '../primitives/Text.cjsx'

export class Player extends React.Component
  componentDidMount: () ->
    player = @props.game.get('player')
    player.on('change', () => @forceUpdate())

  render: () ->
    player = @props.game.get('player')
    playerWeapon = player.get('weapon')
    {x = 0, y = 0} = @props

    return (
      <Container
        x={x}
        y={y}
      >
        <Sprite
          image={player.get('image')}
          scale={0.1}
        />
        {@renderWeapon(playerWeapon)}
        {@renderPlayerInfo(player)}
      </Container>
    )

  renderWeapon: (playerWeapon) ->
    return (
      <Container
        x={40}
        y={0}
      >
        <Sprite
          image={playerWeapon.get('image')}
          scale={0.2}
        />
        <Text
          x={100}
          y={80}
          text={formatNameLevel(playerWeapon)}
        />
      </Container>
    )

  renderPlayerInfo: (player) ->
    <Container
      y={-40}
      x={40}
    >
      <Text
        text={formatNameLevel(player) + ','}
      />
      <Container
        x={120}
        y={0}
      >
        <Sprite
          image={coinImage}
          scale={0.1}
          y={2}
        />
        <Text
          x={30}
          text={player.get('coins')}
        />
      </Container>
    </Container>