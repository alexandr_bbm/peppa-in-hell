import * as React from 'react'
import {joinClassNames} from '../../utils/css'
import styles from './Button.css';

export Button = ({onClick, disabled, className, children}) ->
  disabledClass = if disabled then styles.disabled else ''

  return (
    <div
      className={joinClassNames(styles.root, className, disabledClass)}
      onClick={onClick}
    >
      {children}
      {disabled && <div className={styles.disabledOverlay} />}
    </div>
  )