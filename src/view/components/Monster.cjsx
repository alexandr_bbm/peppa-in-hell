import * as React from 'react'
import {Sprite, Container} from '@inlet/react-pixi';

import {Text} from '../primitives/Text.cjsx'
import {formatNameLevel} from '../../models/utils/formatters'
import {spriteUtils} from '../utils/pixi'
import {HpBar} from './HpBar.cjsx';

export class Monster extends React.Component
  componentDidMount: () ->
    @props.game.on('change:monster', () =>
      @bindMonster()
      @forceUpdate()
    )

    @bindMonster();

  bindMonster: () =>
    monster = @props.game.get('monster')

    monster.on('change', () => @forceUpdate())
    monster.on('attacked', () => spriteUtils.shake(@monsterSprite, 10))

  setMonsterRef: (instance) => @monsterSprite = instance

  onMonsterClick: () =>
    @props.game.attack()

  render: () ->
    monster = @props.game.get('monster')
    {x = 0, y = 0} = @props

    monsterImageWidth = 200
    hpBarWidth = 200

    return (
      <Container
        x={x}
        y={y}
      >
        <Sprite
          image={monster.get('image')}
          width={monsterImageWidth}
          height={200}
          interactive={true}
          pointerdown={@onMonsterClick}
          ref={@setMonsterRef}
        />
        <Text
          x={40}
          y={-50}
          text={formatNameLevel(monster)}
        />
        <HpBar
          hp={monster.get('hp')}
          fullHp={monster.get('fullHp')}
          width={hpBarWidth}
          y={201}
        />
      </Container>
    )