import * as PIXI from 'pixi.js'
import { PixiComponent } from '@inlet/react-pixi'

export default PixiComponent('Rectangle', {
  create: () -> new PIXI.Graphics(),
  applyProps: (g, oldProps, newProps) ->
    { strokeColor, fill, x, y, width, height, strokeWidth = 1 } = newProps
    isFill = fill != undefined

    g.clear()

    g.lineStyle(strokeWidth, strokeColor)

    if isFill
      g.beginFill(fill)

    g.drawRect(x, y, width, height)

    if isFill
      g.endFill()
})