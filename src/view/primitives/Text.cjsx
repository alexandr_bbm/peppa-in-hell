import * as React from 'react'
import {Text as PixiText} from '@inlet/react-pixi';

commonTextStyle =
  fill: '#ffffff'

export Text = ({x = 0, y = 0, text, style}) ->
  <PixiText
    x={x}
    y={y}
    text={text}
    style={{
      style...,
      commonTextStyle...
    }}
  />