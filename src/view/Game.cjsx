import * as React from 'react'
import {Stage} from '@inlet/react-pixi';

import styles from './Game.css';
import {Button} from './components/Button/Button.cjsx'
import {formatNameLevel} from '../models/utils/formatters'
import {spriteUtils} from './utils/pixi'
import {Monster} from "./components/Monster.cjsx"
import {Player} from "./components/Player.cjsx"


export class Game extends React.Component
  componentDidMount: () ->
    @props.game.get('player').on('change:coins', () => @forceUpdate())

  onStageMount: (app) =>
    app.ticker.add(@gameLoop)

  gameLoop: ->
    spriteUtils.update();

  @stageOptions = {
    transparent: true
    width: 960
    height: 600
  }

  render: () ->
    player = @props.game.get('player')

    upgradeButton =
      <Button
        className={styles.upgradeButton}
        disabled={!player.isLevelUpgradePossible()}
        onClick={@onUpgradeClick}
      >
        Upgrade to lvl {player.getNextLevel()} ({player.getNextLevelCost()} coins)
      </Button>

    return (
      <div className={styles.root}>
        {upgradeButton}
        <Stage
          options={Game.stageOptions}
          onMount={@onStageMount}
        >
          <Player
            x={50}
            y={400}
            game={@props.game}
          />
          <Monster
            x={400}
            y={350}
            game={@props.game}
          />
        </Stage>
      </div>
    )

  onUpgradeClick: () =>
    player = @props.game.get('player')
    player.upgradeLevel()

