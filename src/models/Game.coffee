import {Model} from 'backbone-model'
import {Monster, MonsterType, getNextMonsterType} from './Monster';
import {Player} from './Player'

export Game = Model.extend({
  initialize: () ->
    @set({
      monster: new Monster({
        level: 1
        type: MonsterType.HellDog
      })
      player: new Player()
    })

  attack: () ->
    damage = @get('player').getDamage()
    monster = @get('monster')
    monster.set('hp', monster.get('hp') - damage);

    monster.trigger('attacked');

    if !monster.isAlive()
      @grantCoinsForMonsterKill(monster)
      @setNextMonster()

  grantCoinsForMonsterKill: (monster) ->
    player = @get('player')
    player.set(
      'coins',
      player.get('coins') + monster.get('level') * 100
    )

  setNextMonster: () ->
    monster = @get('monster')
    @set('monster', new Monster({
      level: monster.get('level') + 1
      type: getNextMonsterType(monster.get('type'))
    }))
})