import {Model} from 'backbone-model'
import monster1Img from '../img/monster1.png';
import monster2Img from '../img/monster2.png';
import monster3Img from '../img/monster3.png';

export MonsterType =
  HellDog: 'HellDog'
  Predator: 'Predator'
  HellShit: 'HellShit'

export getNextMonsterType = (monsterType) ->
  idx = MonsterTypes.indexOf(monsterType)
  return MonsterTypes[idx + 1] || MonsterTypes[0]

MonsterTypes = Object.values(MonsterType)

MonsterTypesInfo =
  [MonsterType.HellDog]:
    name: 'HellDog'
    image: monster1Img,

  [MonsterType.Predator]:
    name: 'Predator'
    image: monster2Img,

  [MonsterType.HellShit]:
    name: 'HellShit'
    image: monster3Img,

export Monster = Model.extend({
  initialize: () ->
    {level, type} = @attributes
    monsterTypeInfo = MonsterTypesInfo[type]
    fullHp = level * 100

    @set({
      level: level
      type: type
      fullHp: fullHp
      hp: fullHp
      image: monsterTypeInfo.image
      name: monsterTypeInfo.name
    })

  isAlive: () ->
    @get('hp') > 0

})
