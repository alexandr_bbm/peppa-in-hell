import {Model} from 'backbone-model'
import axeImage from '../img/axe.png';

export WeaponType =
  Axe: 'Axe'

WeaponTypesInfo =
  [WeaponType.Axe]:
    name: 'Axe'
    image: axeImage,

export Weapon = Model.extend({
  initialize: () ->
    {type} = @attributes

    {name, image} = WeaponTypesInfo[type]
    @set({
      image: image
      name: name
      level: 1
    })

  getDamage: () ->
    @get('level') * 10
})