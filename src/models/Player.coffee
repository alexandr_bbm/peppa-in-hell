import {Model} from 'backbone-model'
import peppaImage from '../img/peppa.png'
import {Weapon, WeaponType} from './Weapon'

export Player = Model.extend({
  initialize: () ->
    @set({
      image: peppaImage
      level: 1
      coins: 0
      weapon: new Weapon({type: WeaponType.Axe})
      name: 'You'
    })

  getDamage: () ->
    return @get('weapon').getDamage() * @get('level')

  getNextLevel: () -> @get('level') + 1

  getNextLevelCost: () -> @getNextLevel() * 100

  isLevelUpgradePossible: () -> @get('coins') >= @getNextLevelCost()

  upgradeLevel: () ->
    @set('coins', @get('coins') - @getNextLevelCost())
    @set('level', @getNextLevel())
})